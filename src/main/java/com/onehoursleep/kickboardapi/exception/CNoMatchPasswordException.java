package com.onehoursleep.kickboardapi.exception;

public class CNoMatchPasswordException extends RuntimeException {
    public CNoMatchPasswordException(String msg, Throwable t) {
        super(msg, t);
    }
    public CNoMatchPasswordException(String msg) {
        super(msg);
    }

    public CNoMatchPasswordException() {
        super();
    }
}