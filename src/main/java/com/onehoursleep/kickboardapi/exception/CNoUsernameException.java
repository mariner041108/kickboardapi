package com.onehoursleep.kickboardapi.exception;

public class CNoUsernameException extends RuntimeException {
    public CNoUsernameException(String msg, Throwable t) {
        super(msg, t);
    }
    public CNoUsernameException(String msg) {
        super(msg);
    }

    public CNoUsernameException() {
        super();
    }
}