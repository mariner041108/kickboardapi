package com.onehoursleep.kickboardapi.exception;

public class CDuplicateUserNameException extends RuntimeException {
    public CDuplicateUserNameException(String msg, Throwable t) {
        super(msg, t);
    }
    public CDuplicateUserNameException(String msg) {
        super(msg);
    }

    public CDuplicateUserNameException() {
        super();
    }
}