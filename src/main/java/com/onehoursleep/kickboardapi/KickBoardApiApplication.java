package com.onehoursleep.kickboardapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KickBoardApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(KickBoardApiApplication.class, args);
	}

}
