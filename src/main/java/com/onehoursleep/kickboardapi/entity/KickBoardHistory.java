package com.onehoursleep.kickboardapi.entity;

import com.onehoursleep.kickboardapi.interfaces.CommonModelBuilder;
import com.onehoursleep.kickboardapi.model.KickBoardUseEndRequest;
import com.onehoursleep.kickboardapi.model.KickBoardUseStartRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardHistory {
    //시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //회원id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    //킥보드id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kickBoardId", nullable = false)
    private KickBoard kickBoard;

    //시작위도
    @Column(nullable = false)
    private Double startPosX;

    //시작경도
    @Column(nullable = false)
    private Double startPosY;

    //시작시간
    @Column(nullable = false)
    private LocalDateTime dateStart;

    //종료위도
    private Double endPosX;

    //종료경도
    private Double endPosY;

    //종료시간
    private LocalDateTime dateEnd;

    //총합가격
    @Column(nullable = false)
    private Double totalPrice;

    //완료여부
    @Column(nullable = false)
    private Boolean completionStatus;

    public void putEndBaseInfo(KickBoardUseEndRequest kickBoardUseEndRequest) { // 종료 위도, 경도, 시간
        this.endPosX = kickBoardUseEndRequest.getEndPosX();
        this.endPosY = kickBoardUseEndRequest.getEndPosX();
        this.dateEnd = LocalDateTime.now();
    }

    public void putEndPrices(double resultPrice) { // 총합가격
        this.totalPrice = resultPrice;
        this.completionStatus = true;
    }

    private KickBoardHistory(Builder builder) {
        this.member = builder.member; // 회원
        this.kickBoard = builder.kickBoard; // 킥보드
        this.startPosX = builder.startPosX; // 시작위도
        this.startPosY = builder.startPosY; // 시작경도
        this.dateStart = builder.dateStart; // 시작시간
        this.totalPrice = builder.totalPrice; // 총합가격
        this.completionStatus = builder.completionStatus; // 완료여부

    }

    public static class Builder implements CommonModelBuilder<KickBoardHistory> { // 시작처리
        private final Member member;
        private final KickBoard kickBoard;
        private final Double startPosX;
        private final Double startPosY;
        private final LocalDateTime dateStart;
        private final Double totalPrice;
        private final Boolean completionStatus;

        public Builder(Member member, KickBoard kickBoard, KickBoardUseStartRequest request) {
            this.member = member;
            this.kickBoard = kickBoard;
            this.startPosX = request.getStartPosX();
            this.startPosY = request.getStartPosY();
            this.dateStart = LocalDateTime.now();
            this.totalPrice = 0D;
            this.completionStatus = false;
        }


        @Override
        public KickBoardHistory build() {
            return new KickBoardHistory(this);
        }
    }
}
