package com.onehoursleep.kickboardapi.entity;

import com.onehoursleep.kickboardapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AmountUseHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //맴버id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    //충전시간
    @Column(nullable = false)
    private LocalDateTime chargingTime;

    //충전금액
    @Column(nullable = false)
    private Double chargingPay;

    private AmountUseHistory(Builder builder) {
        this.member = builder.member;
        this.chargingTime = builder.chargingTime;
        this.chargingPay = builder.chargingPay;
    }

    public static class Builder implements CommonModelBuilder<AmountUseHistory> {
        private final Member member;
        private final LocalDateTime chargingTime;
        private final Double chargingPay;

        public Builder(Member member, Double chargingPay) {
            this.member = member;
            this.chargingTime = LocalDateTime.now();
            this.chargingPay = 0D;
        }

        @Override
        public AmountUseHistory build() {
            return new AmountUseHistory(this);
        }
    }
}
