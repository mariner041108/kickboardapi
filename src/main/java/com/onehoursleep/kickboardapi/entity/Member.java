package com.onehoursleep.kickboardapi.entity;

import com.onehoursleep.kickboardapi.interfaces.CommonModelBuilder;
import com.onehoursleep.kickboardapi.model.MemberRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    //시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //아이디
    @Column(nullable = false, length = 20, unique = true)
    private String userName;

    //비밀번호
    @Column(nullable = false, length = 20)
    private String password;

    //이름
    @Column(nullable = false, length = 20)
    private String memberName;

    //연락처
    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;

    //면허증번호
    @Column(nullable = false, length = 15, unique = true)
    private String carLicenseNumber;

    //생년월일
    @Column(nullable = false)
    private LocalDate dateBirth;

    // 포인트
    @Column(nullable = false)
    private Double point;

    private Member(Builder builder) {
        this.userName = builder.userName;
        this.password = builder.password;
        this.memberName = builder.memberName;
        this.phoneNumber = builder.phoneNumber;
        this.carLicenseNumber = builder.carLicenseNumber;
        this.dateBirth = builder.dateBirth;
        this.point = builder.point;

    }

    public static class Builder implements CommonModelBuilder<Member> {
        private final String userName;
        private final String password;
        private final String memberName;
        private final String phoneNumber;
        private final String carLicenseNumber;
        private final LocalDate dateBirth;
        private final Double point;
        public Builder(MemberRequest request) {
            this.userName = request.getUserName();
            this.password = request.getPassword();
            this.memberName = request.getMemberName();
            this.phoneNumber = request.getPhoneNumber();
            this.carLicenseNumber = request.getCarLicenseNumber();
            this.dateBirth = request.getDateBirth();
            this.point = request.getPoint();
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
