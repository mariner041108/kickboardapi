package com.onehoursleep.kickboardapi.entity;

import com.onehoursleep.kickboardapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AmountCharging {
    //시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //회원 id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    //잔여금액
    @Column(nullable = false)
    private Double amountPrice;

    private AmountCharging(Builder builder) {
        this.member = builder.member;
        this.amountPrice = builder.amountPrice;
    }
    public void putPriceMinus(double price) {
        this.amountPrice -= price;
    }


    public static class Builder implements CommonModelBuilder<AmountCharging> {
        private final Member member;
        private final Double amountPrice;

        public Builder(Member member) {
            this.member = member;
            this.amountPrice = 0D;
        }

        @Override
        public AmountCharging build() {
            return new AmountCharging(this);
        }
    }
}
