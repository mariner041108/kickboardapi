package com.onehoursleep.kickboardapi.entity;

import com.onehoursleep.kickboardapi.enums.KickBoardStatus;
import com.onehoursleep.kickboardapi.enums.PlanName;
import com.onehoursleep.kickboardapi.interfaces.CommonModelBuilder;
import com.onehoursleep.kickboardapi.model.KickBoardRequest;
import com.onehoursleep.kickboardapi.model.KickBoardUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoard {
    //시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //모델명
    @Column(nullable = false, length = 20)
    private String modelName;

    // 고유번호
    @Column(nullable = false, length = 6, unique = true)
    private String machineUniqueNumber;

    // 플랜명
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    private PlanName planName;

    //구입일
    @Column(nullable = false, length = 15)
    private LocalDate dateBuy;

    //현재위도
    @Column(nullable = false, length = 15)
    private Double posX;

    //현재경도
    @Column(nullable = false)
    private Double posY;

    //사용여부
    @Column(nullable = false)
    private Boolean useStatus;

    //상태
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    private KickBoardStatus kickBoardStatus;

    //메모
    @Column(columnDefinition = "TEXT")
    private String memo;

    public void setStatus(KickBoardStatus kickBoardStatus) {
        this.kickBoardStatus = kickBoardStatus; // 수정할 빌더 생성
    }

    public void putData(KickBoardUpdateRequest request) {
        this.posX = request.getPosX();
        this.posY = request.getPosY();
        this.useStatus = request.getUseStatus();
        this.memo = request.getMemo();
    }

    private KickBoard(Builder builder) {
        this.modelName = builder.modelName;
        this.machineUniqueNumber = builder.machineUniqueNumber;
        this.planName = builder.planName;
        this.dateBuy = builder.dateBuy;
        this.posX = builder.posX;
        this.posY = builder.posY;
        this.useStatus = builder.useStatus;
        this.kickBoardStatus = builder.kickBoardStatus;
    }

    public static class Builder implements CommonModelBuilder<KickBoard> {
        private final String modelName;
        private final String machineUniqueNumber;
        private final PlanName planName;
        private final LocalDate dateBuy;
        private final Double posX;
        private final Double posY;
        private final Boolean useStatus;
        private final KickBoardStatus kickBoardStatus;


        public Builder(KickBoardRequest request) {
            this.modelName = request.getModelName();
            this.machineUniqueNumber = request.getMachineUniqueNumber();
            this.planName = PlanName.FIRST;
            this.dateBuy = request.getDateBuy();
            this.posX = request.getPosX();
            this.posY = request.getPosY();
            this.useStatus = false;
            this.kickBoardStatus = KickBoardStatus.STAND;
        }

        @Override
        public KickBoard build() {
            return new KickBoard(this);
        }
    }
}
