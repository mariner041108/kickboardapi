package com.onehoursleep.kickboardapi.controller;

import com.onehoursleep.kickboardapi.entity.AmountCharging;
import com.onehoursleep.kickboardapi.entity.KickBoard;
import com.onehoursleep.kickboardapi.entity.KickBoardHistory;
import com.onehoursleep.kickboardapi.entity.Member;
import com.onehoursleep.kickboardapi.enums.KickBoardStatus;
import com.onehoursleep.kickboardapi.model.CommonResult;
import com.onehoursleep.kickboardapi.model.KickBoardUseEndRequest;
import com.onehoursleep.kickboardapi.model.KickBoardUseStartRequest;
import com.onehoursleep.kickboardapi.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "킥보드 사용 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/kickBoardUse")
public class KickBoardUseController {

    private final MemberService memberService;
    private final KickBoardUseService kickBoardUseService;
    private final KickBoardService kickBoardService;
    private final AmountChargingService amountChargingService;


    @ApiOperation(value = "킥보드 사용 처리")
    @PostMapping("/start/member-id/{memberId}/kick-board/{kickBoardId}")
    public CommonResult setUseStart(@PathVariable long memberId, @PathVariable long kickBoardId, @RequestBody @Valid KickBoardUseStartRequest request) {
        Member member = memberService.getOriginData(memberId);
        KickBoard kickBoard = kickBoardService.getOriginData(kickBoardId);
        kickBoardUseService.setUseStart(member, kickBoard, request);

        // 킥보드 상태를 사용중으로 변경한다.
        kickBoardService.setStatus(kickBoard, KickBoardStatus.USE);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "킥보드 종료 처리")
    @PutMapping("/end/kick-board/{kickBoardId}")
    public CommonResult putUseEnd(@PathVariable long kickBoardId, @RequestBody @Valid KickBoardUseEndRequest request) {
        // 이용내역을 종료로 마감시킨다.
        KickBoardHistory kickBoardHistory = kickBoardUseService.setUseEnd(kickBoardId, request);

        // 킥보드 상태를 대기로 변경한다.
        kickBoardService.setStatus(kickBoardHistory.getKickBoard(), KickBoardStatus.STAND);
        // KickBoardService한테 Status를 넘겨주고 History 안에 KickBoard 안에 Status STAND로 설정

        // 확정된 이용요금을 회원 잔액에서 차감시킨다.
        amountChargingService.putAmountMinus(kickBoardHistory.getMember(), kickBoardHistory.getTotalPrice());

        return ResponseService.getSuccessResult(); //
    }

}
