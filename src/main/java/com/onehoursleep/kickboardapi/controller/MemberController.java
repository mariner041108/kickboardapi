package com.onehoursleep.kickboardapi.controller;

import com.onehoursleep.kickboardapi.entity.Member;
import com.onehoursleep.kickboardapi.model.*;
import com.onehoursleep.kickboardapi.service.AmountChargingService;
import com.onehoursleep.kickboardapi.service.MemberService;
import com.onehoursleep.kickboardapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "회원 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;
    private final AmountChargingService amountChargingService;

    @ApiOperation(value = "회원 정보 등록")
    @PostMapping("/data")
    public CommonResult setMember(@RequestBody @Valid MemberRequest request) {
        Member member = memberService.setMember(request);
        amountChargingService.setAmount(member);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "로그인")
    @PostMapping("/login")
    public SingleResult<LoginResponse> doLogin(@RequestBody @Valid LoginRequest request) {
        return ResponseService.getSingleResult(memberService.doLogin(request));
    }
}
