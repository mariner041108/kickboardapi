package com.onehoursleep.kickboardapi.controller;

import com.onehoursleep.kickboardapi.entity.KickBoard;
import com.onehoursleep.kickboardapi.model.*;
import com.onehoursleep.kickboardapi.service.KickBoardService;
import com.onehoursleep.kickboardapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/kickBoard")
@Api(tags = "킥보드 정보 관리")
public class KickBoardController {

    private final KickBoardService kickBoardService;

    @ApiOperation(value = "킥보드 정보 등록")
    @PostMapping("/data")
    public CommonResult setKickBoard(@RequestBody @Valid KickBoardRequest request) {
        kickBoardService.setKickBoard(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "킥보드 정보 리스트")
    @GetMapping("/all")
    public ListResult<KickBoardItem> getData() {
        return ResponseService.getListResult(kickBoardService.getData(),true);
    }

    @ApiOperation(value = "킥보드 정보 수정")
    @PutMapping("/kick-board-id/{KickBoardId}")
    public CommonResult putKickBoard(@PathVariable long KickBoardId, @RequestBody @Valid KickBoardUpdateRequest request) {
        kickBoardService.putKickBoard(KickBoardId,request);
        return ResponseService.getSuccessResult();
    }
}
