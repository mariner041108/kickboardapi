package com.onehoursleep.kickboardapi.model;
import com.onehoursleep.kickboardapi.entity.Member;
import com.onehoursleep.kickboardapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse {
    private Long id;
    private String memberName;

    private LoginResponse(Builder builder) {
        this.id = builder.id;
        this.memberName = builder.memberName;
    }

    public static class Builder implements CommonModelBuilder<LoginResponse> {
        private final Long id;
        private final String memberName;

        public Builder(Member member) {
            this.id = member.getId();
            this.memberName = member.getMemberName();
        }

        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}
