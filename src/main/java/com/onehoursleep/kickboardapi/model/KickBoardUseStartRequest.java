package com.onehoursleep.kickboardapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class KickBoardUseStartRequest {
    //시작위도
    @ApiModelProperty(notes = "시작위도")
    @NotNull
    private Double startPosX;

    //시작경도
    @ApiModelProperty(notes = "시작경도")
    @NotNull
    private Double startPosY;
}
