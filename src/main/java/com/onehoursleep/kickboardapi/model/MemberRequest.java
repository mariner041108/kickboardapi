package com.onehoursleep.kickboardapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberRequest {
    //아이디
    @ApiModelProperty(notes = "아이디")
    @NotNull
    @Length(min = 2, max = 20)
    private String userName;

    //비밀번호
    @ApiModelProperty(notes = "비밀번호")
    @Column(nullable = false, length = 20)
    @NotNull
    @Length(min = 8, max = 20)
    private String password;

    //이름
    @ApiModelProperty(notes = "이름")
    @NotNull
    @Length(min = 2, max = 20)
    private String memberName;

    //연락처
    @ApiModelProperty(notes = "연락처")
    @NotNull
    @Length(min = 13, max = 13)
    private String phoneNumber;

    //면허증번호
    @ApiModelProperty(notes = "면허증번호")
    @NotNull
    @Length(min = 15, max = 15)
    private String carLicenseNumber;

    //생년월일
    @ApiModelProperty(notes = "생년월일")
    @NotNull
    private LocalDate dateBirth;

    @ApiModelProperty(notes = "포인트")
    @NotNull
    private Double point;
}
