package com.onehoursleep.kickboardapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class KickBoardRequest {
    //모델명
    @ApiModelProperty(notes = "모델명")
    @NotNull
    @Length(min = 5, max = 20)
    private String modelName;

    @ApiModelProperty(notes = "고유번호")
    @NotNull
    @Length(min = 6, max = 6)
    private String machineUniqueNumber;

    @ApiModelProperty(notes = "구입일")
    @NotNull
    private LocalDate dateBuy;

    @ApiModelProperty(notes = "현재 위도")
    @NotNull
    private Double posX;

    @ApiModelProperty(notes = "현재 경도")
    @NotNull
    private Double posY;
}
