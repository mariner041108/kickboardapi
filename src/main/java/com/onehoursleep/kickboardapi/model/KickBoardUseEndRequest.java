package com.onehoursleep.kickboardapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class KickBoardUseEndRequest {
    @NotNull
    @ApiModelProperty(notes = "종료위도")
    private Double endPosX;

    @NotNull
    @ApiModelProperty(notes = "종료경도")
    private Double endPosY;
}
