package com.onehoursleep.kickboardapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class KickBoardUpdateRequest {
    @ApiModelProperty(notes = "현재위도")
    @NotNull
    private Double posX;

    @ApiModelProperty(notes = "현재경도")
    @NotNull
    private Double posY;

    @ApiModelProperty(notes = "사용여부")
    @NotNull
    private Boolean useStatus;

    @ApiModelProperty(notes = "메모")
    private String memo;
}
