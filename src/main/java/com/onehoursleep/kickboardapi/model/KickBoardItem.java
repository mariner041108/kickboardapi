package com.onehoursleep.kickboardapi.model;

import com.onehoursleep.kickboardapi.entity.KickBoard;
import com.onehoursleep.kickboardapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardItem {

    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "고유번호")
    private String machineUniqueNumber;

    @ApiModelProperty(notes = "상태")
    private String kickBoardStatus;

    private KickBoardItem(Builder builder) {
        this.id = builder.id;
        this.machineUniqueNumber = builder.machineUniqueNumber;
        this.kickBoardStatus = builder.kickBoardStatus;
    }
    public static class Builder implements CommonModelBuilder<KickBoardItem> {
        private final Long id;
        private final String machineUniqueNumber;
        private final String kickBoardStatus;

        public Builder(KickBoard kickBoard) {
            this.id = kickBoard.getId();
            this.machineUniqueNumber = kickBoard.getMachineUniqueNumber();
            this.kickBoardStatus = kickBoard.getKickBoardStatus().getName();
        }

        @Override
        public KickBoardItem build() {
            return new KickBoardItem(this);
        }
    }
}
