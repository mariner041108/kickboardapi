package com.onehoursleep.kickboardapi.advice;

import com.onehoursleep.kickboardapi.enums.ResultCode;
import com.onehoursleep.kickboardapi.exception.*;
import com.onehoursleep.kickboardapi.model.CommonResult;
import com.onehoursleep.kickboardapi.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CWrongPhoneNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongPhoneNumberException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PHONE_NUMBER);
    }

    @ExceptionHandler(CDuplicateUserNameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDuplicateUserNameException e) {
        return ResponseService.getFailResult(ResultCode.DUPLICATE_USERNAME);
    }

    @ExceptionHandler(CNoUsernameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoUsernameException e) {
        return ResponseService.getFailResult(ResultCode.NO_USERNAME);
    }

    @ExceptionHandler(CNoMatchPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoMatchPasswordException e) {
        return ResponseService.getFailResult(ResultCode.NO_MATCH_PASSWORD);
    }

}
