package com.onehoursleep.kickboardapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
