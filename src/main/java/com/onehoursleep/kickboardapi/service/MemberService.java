package com.onehoursleep.kickboardapi.service;

import com.onehoursleep.kickboardapi.entity.Member;
import com.onehoursleep.kickboardapi.exception.CDuplicateUserNameException;
import com.onehoursleep.kickboardapi.exception.CMissingDataException;
import com.onehoursleep.kickboardapi.exception.CNoMatchPasswordException;
import com.onehoursleep.kickboardapi.exception.CNoUsernameException;
import com.onehoursleep.kickboardapi.model.LoginRequest;
import com.onehoursleep.kickboardapi.model.LoginResponse;
import com.onehoursleep.kickboardapi.model.MemberRequest;
import com.onehoursleep.kickboardapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getOriginData(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public Member setMember(MemberRequest request) {
        if (!isNewUsername(request.getUserName())) throw new CDuplicateUserNameException();

        Member member = new Member.Builder(request).build();
        return memberRepository.save(member);
    }

    private boolean isNewUsername(String username) {
        long duplicateCount = memberRepository.countByUserName(username);

        return duplicateCount <= 0;
    }

    public LoginResponse doLogin(LoginRequest request) {
        Member member = memberRepository.findByUserName(request.getUsername()).orElseThrow(CNoUsernameException::new);
        if (!member.getPassword().equals(request.getPassword())) throw new CNoMatchPasswordException();

        return new LoginResponse.Builder(member).build();
    }
}
