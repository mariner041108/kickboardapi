package com.onehoursleep.kickboardapi.service;

import com.onehoursleep.kickboardapi.entity.KickBoard;
import com.onehoursleep.kickboardapi.entity.KickBoardHistory;
import com.onehoursleep.kickboardapi.entity.Member;
import com.onehoursleep.kickboardapi.enums.PlanName;
import com.onehoursleep.kickboardapi.exception.CMissingDataException;
import com.onehoursleep.kickboardapi.model.KickBoardUseEndRequest;
import com.onehoursleep.kickboardapi.model.KickBoardUseStartRequest;
import com.onehoursleep.kickboardapi.repository.KickBoardHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;

@Service
@RequiredArgsConstructor
public class KickBoardUseService {
    private final KickBoardHistoryRepository kickBoardHistoryRepository;

    public void setUseStart(Member member, KickBoard kickBoard, KickBoardUseStartRequest request) {
        KickBoardHistory kickBoardHistory = new KickBoardHistory.Builder(member, kickBoard, request).build();
        kickBoardHistoryRepository.save(kickBoardHistory);
    }
    public KickBoardHistory setUseEnd(long id, KickBoardUseEndRequest kickBoardUseEndRequest) {
        // 3. 이용내역id를 기준으로 데이터를 가져온다.
        KickBoardHistory kickBoardHistory = kickBoardHistoryRepository.findById(id).orElseThrow(CMissingDataException::new);

        // 4. 3번에서 가져온 데이터에 종료 위도, 종료 경도, 종료 시간을 넣는다.
        kickBoardHistory.putEndBaseInfo(kickBoardUseEndRequest);

        // 6. 킥보드 테이블의 기준요금표를 가져온다.
        PlanName planName = kickBoardHistory.getKickBoard().getPlanName();

        // 7. 기본요금
        double basePrice = planName.getPlanBasicPrice();
        // 종료시작 - 시작시간 : 초
        long totalSecond = ChronoUnit.SECONDS.between(kickBoardHistory.getDateStart(), kickBoardHistory.getDateEnd());
        // 초를 분으로 변환
        double perMin = Math.ceil(totalSecond / 60.0);
        // 분당추가요금 계산
        double addPrice = perMin * planName.getPerMinPrice();
        // 최종 이용 요금
        double totalPrice = basePrice + addPrice;

        // 이용요금을 이용내역에 기록 + 이용완료로 처리
        kickBoardHistory.putEndPrices(totalPrice);

        // 이 팀은 이용내역만 관리하는 팀이라서 회원 잔액조정이나 킥보드상태를 변경할 수 없으니 Member와 KickBoard 정보를 모두 담고있는 KickBoardHistory
        // 엔티티 전체를 넘겨 준다. (컨트롤러에서 알아서 해라 시전)
        return kickBoardHistoryRepository.save(kickBoardHistory);
    }

}
