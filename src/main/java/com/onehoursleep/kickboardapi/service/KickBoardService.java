package com.onehoursleep.kickboardapi.service;

import com.onehoursleep.kickboardapi.entity.KickBoard;
import com.onehoursleep.kickboardapi.enums.KickBoardStatus;
import com.onehoursleep.kickboardapi.enums.PlanName;
import com.onehoursleep.kickboardapi.exception.CMissingDataException;
import com.onehoursleep.kickboardapi.model.KickBoardItem;
import com.onehoursleep.kickboardapi.model.KickBoardRequest;
import com.onehoursleep.kickboardapi.model.KickBoardUpdateRequest;
import com.onehoursleep.kickboardapi.model.ListResult;
import com.onehoursleep.kickboardapi.repository.KickBoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class KickBoardService {
    private final KickBoardRepository kickBoardRepository;



    public KickBoard getOriginData(long id) {
        return kickBoardRepository.findById(id).orElseThrow(CMissingDataException::new);
        // id를 건네주고 그에 맞는 데이터가 없을 시 CMissing에 저장된 enum값을 보여준다.
    }
    public void setKickBoard(KickBoardRequest request) {
        KickBoard kickBoard = new KickBoard.Builder(request).build();
        kickBoardRepository.save(kickBoard);
    }

    public ListResult<KickBoardItem> getData() {
        List<KickBoard> originData = kickBoardRepository.findAll();
        List<KickBoardItem> result = new LinkedList<>();
        originData.forEach(e -> result.add(new KickBoardItem.Builder(e).build()));
        return ListConvertService.settingResult(result);
    }

    public void putKickBoard(long id, KickBoardUpdateRequest updateRequest) {
        KickBoard kickBoard = kickBoardRepository.findById(id).orElseThrow(CMissingDataException::new);
        kickBoard.putData(updateRequest);
        kickBoardRepository.save(kickBoard);
    }

    public void setStatus(KickBoard kickBoard, KickBoardStatus kickBoardStatus) {
        kickBoard.setStatus(kickBoardStatus);
        kickBoardRepository.save(kickBoard);
    }


}
