package com.onehoursleep.kickboardapi.service;

import com.onehoursleep.kickboardapi.entity.AmountCharging;
import com.onehoursleep.kickboardapi.entity.Member;
import com.onehoursleep.kickboardapi.repository.AmountChargingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AmountChargingService {
    private final AmountChargingRepository amountChargingRepository;

    public void setAmount(Member member) {
        AmountCharging amountCharging = new AmountCharging.Builder(member).build();
        amountChargingRepository.save(amountCharging);
    }

    public void putAmountMinus(Member member, double price) {

        AmountCharging amountCharging = amountChargingRepository.findByMember(member);

        amountCharging.putPriceMinus(price);

        amountChargingRepository.save(amountCharging);
    }
}