package com.onehoursleep.kickboardapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum KickBoardStatus {
    USE("사용중"),
    STAND("대기중"),
    CHECK("점검중");

    private final String name;
}
