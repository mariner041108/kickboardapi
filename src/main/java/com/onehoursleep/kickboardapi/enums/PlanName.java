package com.onehoursleep.kickboardapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PlanName {
    THIRD("삼애플랜", "1000000", "1000000", "200000", "500000", 800D, 150D),
    SECOND("차애플랜","500000", "500000", "200000", "100000", 1000D, 150D),
    FIRST("최애플랜", "300000", "300000", "200000", "면책", 1200D, 150D);

    private final String name; // 이름
    private final String personalCompensation; // 대인손해배상
    private final String propertiCompnsation; // 대물손해배상
    private final String selfCompensation; // 자기손해배상
    private final String deviceCompensation; // 기기손해배상
    private final Double planBasicPrice; // 플랜기본가격
    private final Double PerMinPrice; // 분당 추가요금
}