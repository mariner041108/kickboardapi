package com.onehoursleep.kickboardapi.repository;

import com.onehoursleep.kickboardapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {

    long countByUserName(String username);

    Optional<Member> findByUserName(String username);
}