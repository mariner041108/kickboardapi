package com.onehoursleep.kickboardapi.repository;

import com.onehoursleep.kickboardapi.entity.KickBoard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KickBoardRepository extends JpaRepository<KickBoard, Long> {
}
