package com.onehoursleep.kickboardapi.repository;

import com.onehoursleep.kickboardapi.entity.KickBoardHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KickBoardHistoryRepository extends JpaRepository<KickBoardHistory, Long> {
}
