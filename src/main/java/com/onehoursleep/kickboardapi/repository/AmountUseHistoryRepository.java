package com.onehoursleep.kickboardapi.repository;

import com.onehoursleep.kickboardapi.entity.AmountUseHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AmountUseHistoryRepository extends JpaRepository<AmountUseHistory, Long> {
}
