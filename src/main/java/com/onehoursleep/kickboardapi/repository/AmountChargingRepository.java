package com.onehoursleep.kickboardapi.repository;

import com.onehoursleep.kickboardapi.entity.AmountCharging;
import com.onehoursleep.kickboardapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AmountChargingRepository extends JpaRepository<AmountCharging, Long> {
    AmountCharging findByMember (Member member); // member는 받아올 것의 타입
    // 원본의 타입으로 먼저 받고 findBy한 자료의 타입을 뒤에 적어준다.
}
